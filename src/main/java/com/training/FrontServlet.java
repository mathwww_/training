package com.training;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FrontServlet
 */
@WebServlet("/home")
public class FrontServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private String myString = "";
    
    public void init(ServletConfig config) throws ServletException {
    	myString = "Hello guys. Welcome to my servlet !";

		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			String url = "jdbc:mariadb://localhost:8306/training";
			String userName = "training";
			String password = "complicatedpassword123";
			Connection con = DriverManager.getConnection(url, userName, password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from USER_ONE");

			while (rs.next()) {
				System.out.println(rs.getString(1) + "  " + rs.getString(2));
			}

			con.close();
			

		} catch (ClassNotFoundException | SQLException e) {

			e.printStackTrace();
		}
    }
	
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 request.setAttribute("value", myString);
	        request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
